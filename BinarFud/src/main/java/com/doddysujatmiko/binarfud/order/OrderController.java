package com.doddysujatmiko.binarfud.order;

import com.doddysujatmiko.binarfud.order.dtos.CreateOrderDTO;
import com.doddysujatmiko.binarfud.utils.Responser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<?> getOrder(Principal principal) {
        return Responser.constructSuccess(orderService.read(principal), "Get order", HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> postOrder(@RequestBody CreateOrderDTO req,
                                       Principal principal) {
        return Responser.constructSuccess(orderService.create(req, principal), "Order created", HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteOrder(@RequestParam(name = "orderId") Long orderId,
                                         Principal principal) {
        orderService.delete(orderId, principal);
        return Responser.constructSuccess("", "Order created", HttpStatus.OK);
    }
}
