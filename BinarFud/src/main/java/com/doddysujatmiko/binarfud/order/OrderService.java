package com.doddysujatmiko.binarfud.order;

import com.doddysujatmiko.binarfud.auth.UserRepository;
import com.doddysujatmiko.binarfud.exceptions.ForbiddenException;
import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import com.doddysujatmiko.binarfud.exceptions.UnathorizedException;
import com.doddysujatmiko.binarfud.order.dtos.CreateOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;

    @Autowired
    private UserRepository userRepository;

    public OrderEntity create(CreateOrderDTO req, Principal principal) {
        var order = new OrderEntity();
        order.setDestinationAddress(req.getDestinationAddress());
        order.setIsCompleted(req.getIsCompleted());
        order.setUser(userRepository.findByUsername(principal.getName()).get());

        return orderRepository.save(order);
    }

    public List<OrderEntity> read(Principal principal) {
        return orderRepository.findAllByUser(userRepository.findByUsername(principal.getName()).get());
    }

    public void delete(Long id, Principal principal) {
        var order = orderRepository.findById(id);

        if(order.isEmpty()) {
            throw new NotFoundException("Order doesn't exist");
        }

        if(!order.get().getUser().getUsername().equals(principal.getName())) {
            throw new ForbiddenException("This order is not yours");
        }

        orderRepository.delete(order.get());
    }
}
