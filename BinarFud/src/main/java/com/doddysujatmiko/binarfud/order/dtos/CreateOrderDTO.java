package com.doddysujatmiko.binarfud.order.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateOrderDTO {
    @NotBlank
    private String destinationAddress;

    @NotNull
    private Boolean isCompleted;
}
