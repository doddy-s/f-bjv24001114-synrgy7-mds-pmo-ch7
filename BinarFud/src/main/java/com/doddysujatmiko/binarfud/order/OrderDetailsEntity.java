package com.doddysujatmiko.binarfud.order;

import com.doddysujatmiko.binarfud.product.ProductEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table
public class OrderDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer quantity;

    @JsonIgnore
    @ManyToOne(targetEntity = ProductEntity.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name="product_id", referencedColumnName = "id")
    private ProductEntity product;

    @JsonIgnore
    @ManyToOne(targetEntity = OrderEntity.class, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name="order_id", referencedColumnName = "id")
    private OrderEntity order;

}
