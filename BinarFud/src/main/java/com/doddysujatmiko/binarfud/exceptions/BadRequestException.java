package com.doddysujatmiko.binarfud.exceptions;

public class BadRequestException extends RuntimeException{
    public BadRequestException(String s) {
        super(s);
    }
}