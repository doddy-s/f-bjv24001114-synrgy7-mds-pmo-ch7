package com.doddysujatmiko.binarfud.exceptions;

import com.doddysujatmiko.binarfud.utils.Responser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(Exception exception) {
        return Responser.constructError(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleBadRequestException(Exception exception) {
        return Responser.constructError(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnathorizedException.class)
    public ResponseEntity<?> handleUnauthorizedException(Exception exception) {
        return Responser.constructError(exception.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<?> handleForbiddenException(Exception exception) {
        return Responser.constructError(exception.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception exception) {
        return Responser.constructError(exception.getMessage() + "---->[EXCEPTION]", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Error.class)
    public ResponseEntity<?> handleError(Error error) {
        return Responser.constructError(error.getMessage() + "---->[ERROR]", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
