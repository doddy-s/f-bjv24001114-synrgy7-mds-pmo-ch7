package com.doddysujatmiko.binarfud.auth;

import com.doddysujatmiko.binarfud.utils.Responser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authtest")
public class AuthTestController {

    @GetMapping("/user")
    public ResponseEntity<?> user(Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();
        return Responser.constructSuccess(user, "You're authed as user", HttpStatus.OK);
    }

    @GetMapping("/admin")
    public ResponseEntity<?> admin(Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();
        return Responser.constructSuccess(user, "You're authed as user", HttpStatus.OK);
    }

    @GetMapping("/superuser")
    public ResponseEntity<?> superuser(Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();
        return Responser.constructSuccess(user, "You're authed as user", HttpStatus.OK);
    }

    @GetMapping("/google")
    public ResponseEntity<?> google(Authentication authentication) {
        UserEntity user = (UserEntity) authentication.getPrincipal();
        return Responser.constructSuccess(user, "You're authed as user", HttpStatus.OK);
    }
}
