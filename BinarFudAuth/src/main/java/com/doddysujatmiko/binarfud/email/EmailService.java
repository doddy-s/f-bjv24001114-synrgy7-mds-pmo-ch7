package com.doddysujatmiko.binarfud.email;

import com.doddysujatmiko.binarfud.email.dtos.EmailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    public void sendSimpleMail(EmailDTO email)
    {
        SimpleMailMessage mailMessage
                = new SimpleMailMessage();

        mailMessage.setFrom(sender);
        mailMessage.setTo(email.getRecipient());
        mailMessage.setText(email.getMsgBody());
        mailMessage.setSubject(email.getSubject());

        javaMailSender.send(mailMessage);
    }
}
