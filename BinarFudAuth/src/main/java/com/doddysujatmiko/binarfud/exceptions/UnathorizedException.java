package com.doddysujatmiko.binarfud.exceptions;

public class UnathorizedException extends RuntimeException{
    public UnathorizedException(String s) {
        super(s);
    }
}
