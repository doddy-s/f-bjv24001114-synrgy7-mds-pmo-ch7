package com.doddysujatmiko.binarfud.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Component
@Service
public class AuthSeeder implements ApplicationRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    RoleRepository roleRepository;

    private final String[] roles = {
            "ROLE_USER",
            "ROLE_ADMIN",
            "ROLE_SUPERUSER"
    };

    private final String[] users = {
            "user user ROLE_USER",
            "admin admin ROLE_ADMIN",
            "superuser superuser ROLE_SUPERUSER"
    };

    @Override
    public void run(ApplicationArguments args) {
        insertRoles();
        insertUsers();
    }

    private void insertRoles() {
        for(String role : roles) {
            if(roleRepository.existsByName(role)) {
                continue;
            }

            RoleEntity roleEntity = new RoleEntity(role);
            roleRepository.save(roleEntity);
        }
    }

    private void insertUsers() {
        for(String user : users) {
            String[] strings = user.split(" ");

            if(userRepository.existsByUsername(strings[0])) {
                continue;
            }

            UserEntity userEntity = new UserEntity(strings[0], strings[1]);

            userEntity.setRoles(roleRepository.findByNameIn(Collections.singleton(strings[2])));

            authService.createUser(userEntity);
        }
    }
}
