package com.doddysujatmiko.binarfud.auth.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Oauth2UserInfoDTO {

    private String name;
    private String id;
    private String email;
    private String picture;
}