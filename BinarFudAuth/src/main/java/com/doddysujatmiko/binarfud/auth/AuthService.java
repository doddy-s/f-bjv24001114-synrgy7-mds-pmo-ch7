package com.doddysujatmiko.binarfud.auth;

import com.doddysujatmiko.binarfud.auth.dtos.ResetPasswordDTO;
import com.doddysujatmiko.binarfud.email.dtos.EmailDTO;
import com.doddysujatmiko.binarfud.email.EmailService;
import com.doddysujatmiko.binarfud.exceptions.BadRequestException;
import com.doddysujatmiko.binarfud.exceptions.NotFoundException;
import com.doddysujatmiko.binarfud.utils.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService implements UserDetailsManager {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmailService emailService;

    @Value("baseurl")
    private String baseUrl;

    @Override
    public void createUser(UserDetails user) {
        ((UserEntity) user).setPassword(passwordEncoder.encode(user.getPassword()));

        if(userRepository.existsByUsername(user.getUsername())) {
            throw new BadRequestException("User already exists");
        }

        userRepository.save((UserEntity) user);
    }

    @Override
    public void updateUser(UserDetails user) {

    }

    @Override
    public void deleteUser(String username) {

    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {

    }

    @Override
    public boolean userExists(String username) {
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> optionalUser = userRepository.findByUsername(username);

        if(optionalUser.isEmpty()) {
            throw new NotFoundException("User doesn't exist");
        }

        return optionalUser.get();
    }

    public UserEntity loadUserByEmail(String email) throws UsernameNotFoundException {
        Optional<UserEntity> optionalUser = userRepository.findByEmail(email);

        if(optionalUser.isEmpty()) {
            throw new NotFoundException("User doesn't exist");
        }

        return optionalUser.get();
    }

    public UserEntity loadUserWithOTP(String username, String otp) {
        var user = userRepository.findByUsername(username);

        if(user.isEmpty()) {
            throw new NotFoundException("User doesn't exist");
        }

        if(!user.get().getOtp().equals(otp)) {
            throw new BadRequestException("Wrong OTP");
        }

        user.ifPresent(u -> u.setOtp(null));

        userRepository.save(user.get());

        return user.get();
    }

    public void sendRecoveryEmail(String email) {
        var user = userRepository.findByEmail(email);

        if(user.isEmpty()) {
            throw new NotFoundException("User doesn't exist");
        }

        String otp = Generator.generateOTP(16);
        user.ifPresent(u -> u.setOtp(otp));
        userRepository.save(user.get());

        EmailDTO recoveryEmail = new EmailDTO();
        recoveryEmail.setRecipient(user.get().getEmail());
        recoveryEmail.setSubject("Your BinarFud Password Recovery");
        recoveryEmail.setMsgBody("Your OTP = " + otp);

        emailService.sendSimpleMail(recoveryEmail);
    }

    public void resetPassword(String otp, ResetPasswordDTO req) {
        Optional<UserEntity> user;

        if(req.getUsername().isEmpty()) {
            user = userRepository.findByEmail(req.getEmail());
        } else {
            user = userRepository.findByUsername(req.getUsername());
        }

        if(user.isEmpty()) {
            throw new NotFoundException("User doesn't exist");
        }

        if(!user.get().getOtp().equals(otp)) {
            throw new BadRequestException("Wrong OTP");
        }

        user.ifPresent(u -> u.setPassword(passwordEncoder.encode(req.getPassword())));
        user.ifPresent(u -> u.setOtp(null));
        userRepository.save(user.get());
    }
}
