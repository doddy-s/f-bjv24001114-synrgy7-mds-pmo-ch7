package com.doddysujatmiko.binarfudgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinarFudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(BinarFudGatewayApplication.class, args);
    }

}
