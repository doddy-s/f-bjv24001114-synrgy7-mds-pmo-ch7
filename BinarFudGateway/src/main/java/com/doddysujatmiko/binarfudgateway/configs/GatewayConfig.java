package com.doddysujatmiko.binarfudgateway.configs;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator routerBuilder(RouteLocatorBuilder routeLocatorBuilder){
        return routeLocatorBuilder.routes()
                .route("BinarFudAuth",r->r.path("/auth/**")
                        .filters(f->f.rewritePath("/auth/(?<suburl>.*)", "/api/${suburl}"))
                        .uri("http://localhost:8081/"))
                .route("BinarFud",r->r.path("/main/**")
                        .filters(f->f.rewritePath("/main/(?<suburl>.*)", "/api/${suburl}"))
                        .uri("http://localhost:8080/"))
                .build();
    }

}